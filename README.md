# ESP32-LED-MATRIX-DRIVER V1.1

ESP32 LED Matrix Driver for RGB LED Matrix Panel

v1.0 tested with [this fork](https://github.com/kkaoo/PxMatrix) of [PxMatrix](https://github.com/2dom/PxMatrix)
that supports 64 x 64 Panels with FM6126A IC that[ I have](https://www.aliexpress.com/item/32650825019.html),
and [Arduino Core for ESP32](https://github.com/espressif/arduino-esp32)

5V power is supplied to the ESP32 and the Led Matrix only from the DC Power Jack.
The Micro-USB will only powers the ESP32 (if connected).

The Leds in the panel are are connected to multiple [Shift Registers](https://lastminuteengineers.com/74hc595-shift-register-arduino-tutorial/).  
You can read a good and simple explanation of how these panels work [here](https://www.bigmessowires.com/2018/05/24/64-x-32-led-matrix-programming/)


**Changes v1.1**
*  Reposition of IDC (J5) connector and the power on leds (it collide with J3)
*  Added 'wings' to the PCB. It should make the disconnection from the panel easier.
*  Added more text markings on the PCB
*  C11 size was 0805, fixed to the correct size which is 1206
*  Added missing schottky diode in the voltage source selector for the regulator

**Known Problems**
* When the 5V power is connected to the Power Jack, it's not possible
  to program the ESP32. I yet didn't try to figure out why.

![Led Matrix Back v1.0](Images/MATRIX_BACK.jpg)

**Shortened BOM**

| Ref. | Name / Link |
| ------ | ------ |
| J1 | [Micro USB](https://www.aliexpress.com/item/32942848561.html) |
| J2 | [DC Power Jack](https://www.aliexpress.com/item/32361314769.html)  |
| J3 | [KF7.62 - 2P](https://www.aliexpress.com/item/32924525467.html) |
| J4 | [2X8 2.54mm Double Row Female Header](https://www.aliexpress.com/item/32660610989.html) |
| J5 | [16 Pin IDC Socket](https://www.aliexpress.com/item/1719598670.html) |
| SW 1 | [Tactile Push Button](https://www.aliexpress.com/item/32623940621.html) |
| U1 | [UMH3N](https://www.aliexpress.com/item/32848670397.html) |
| U2 | [CH340C](https://www.aliexpress.com/item/32951382284.html) |
| U3 | [ME6211](https://www.aliexpress.com/item/32821364365.html) |
| U4 | [ESP32-WROOM-32](https://www.aliexpress.com/item/32919183232.html) |
| Q1 | [AO3401](https://www.aliexpress.com/item/32359403044.html?) |

![3D Top](Images/3d_top.png)

## Connections

The display is actually 2 displays one after the other.  
R1 B1 G1 - are the inputs for the shift registers of the upper half.  
R2 B2 G2 - are the input for the shift registes of the lower half.  
I daisy-chained the outputs to the inputs of the registers, so now I have one big shift register.  
R1 -> R2 -> G1 -> G2 -> B1 -> B2
 
The input for the circuit is R1.

**PI and PO**

  PI | PO
  ---|---
  R2 | R1
  G1 | R2
  G2 | G1
  B1 | G2
  B2 | B1

**ESP32**

  PI  | ESP32 |
  ----|----|
  STB/LAT | 22 |
  A  | 19 |
  B  | 23 |
  C  | 18 |
  D  | 5  |
  E  | 15 |
  OE | 2 |
  CK | 14 |
  R1  | 13 |

![Spotify Album Art Cover](Images/SPOTIFY_COVER.jpg)

